provider "panos" {
  hostname = "${data.terraform_remote_state.core.outputs.MGT-IP-FW-2}"
  username = "admin"
  password = "Pal0Alt0@123"
}

resource "panos_management_profile" "imp_allow_ping" {
  name = "Allow ping"
  ping = true
}

resource "panos_ethernet_interface" "eth1_1" {
  name                      = "ethernet1/1"
  vsys                      = "vsys1"
  mode                      = "layer3"
  comment                   = "External interface"
  enable_dhcp               = true
  create_dhcp_default_route = true
}

resource "panos_ethernet_interface" "eth1_2" {
  name        = "ethernet1/2"
  vsys        = "vsys1"
  mode        = "layer3"
  comment     = "Web interface"
  enable_dhcp = true
}

resource "panos_zone" "zone_untrust" {
  name       = "UNTRUST"
  mode       = "layer3"
  interfaces = ["${panos_ethernet_interface.eth1_1.name}"]
}

resource "panos_zone" "zone_trust" {
  name       = "TRUST"
  mode       = "layer3"
  interfaces = ["${panos_ethernet_interface.eth1_2.name}"]
}

resource "panos_service_object" "so_22" {
  name             = "service-tcp-22"
  protocol         = "tcp"
  destination_port = "22"
}

resource "panos_service_object" "so_81" {
  name             = "service-http-81"
  protocol         = "tcp"
  destination_port = "81"
}

resource "panos_service_object" "so_8083" {
  name             = "service-tl-8083"
  protocol         = "tcp"
  destination_port = "8083"
}

resource "panos_service_object" "so_8084" {
  name             = "service-tl-8084"
  protocol         = "tcp"
  destination_port = "8084"
}
resource "panos_security_policies" "security_policies" {
  rule {
    name                  = "SSH inbound"
    source_zones          = ["${panos_zone.zone_untrust.name}"]
    source_addresses      = ["any"]
    source_users          = ["any"]
    hip_profiles          = ["any"]
    destination_zones     = ["${panos_zone.zone_trust.name}"]
    destination_addresses = ["any"]
    applications          = ["ssh"]
    services              = ["application-default"]
    categories            = ["any"]
    action                = "allow"
  }

  rule {
    name                  = "TwistLock"
    source_zones          = ["${panos_zone.zone_trust.name}"]
    source_addresses      = ["any"]
    source_users          = ["any"]
    hip_profiles          = ["any"]
    destination_zones     = ["${panos_zone.zone_untrust.name}"]
    destination_addresses = ["any"]
    applications          = ["ssl"]
    services              = ["service-tl-8083","service-tl-8084"]
    categories            = ["any"]
    action                = "allow"
  }

  rule {
    name                  = "Allow all ping"
    source_zones          = ["any"]
    source_addresses      = ["any"]
    source_users          = ["any"]
    hip_profiles          = ["any"]
    destination_zones     = ["any"]
    destination_addresses = ["any"]
    applications          = ["ping"]
    services              = ["application-default"]
    categories            = ["any"]
    action                = "allow"
  }

  rule {
    name                  = "Web browsing"
    source_zones          = ["${panos_zone.zone_untrust.name}"]
    source_addresses      = ["any"]
    source_users          = ["any"]
    hip_profiles          = ["any"]
    destination_zones     = ["${panos_zone.zone_trust.name}"]
    destination_addresses = ["any"]
    applications          = ["web-browsing"]
    services              = ["service-http", "${panos_service_object.so_81.name}"]
    categories            = ["any"]
    action                = "allow"
  }

  rule {
    name                  = "Allow all outbound"
    source_zones          = ["${panos_zone.zone_trust.name}"]
    source_addresses      = ["any"]
    source_users          = ["any"]
    hip_profiles          = ["any"]
    destination_zones     = ["${panos_zone.zone_untrust.name}"]
    destination_addresses = ["any"]
    applications          = ["any"]
    services              = ["application-default"]
    categories            = ["any"]
    action                = "allow"
  }
  rule {
    name                  = "Log default deny"
    source_zones          = ["any"]
    source_addresses      = ["any"]
    source_users          = ["any"]
    hip_profiles          = ["any"]
    destination_zones     = ["any"]
    destination_addresses = ["any"]
    applications          = ["any"]
    services              = ["application-default"]
    categories            = ["any"]
    action                = "deny"
  }

  depends_on = ["panos_service_object.so_8083","panos_service_object.so_8084","panos_service_object.so_22","panos_service_object.so_81"]

}

resource "panos_nat_rule_group" "top" {
    rule {
        name = "first"
        original_packet {
            source_zones = ["${panos_zone.zone_untrust.name}"]
            destination_zone = "${panos_zone.zone_untrust.name}"
            destination_interface = "${panos_ethernet_interface.eth1_1.name}"
            source_addresses = ["any"]
            service = "service-http"
            destination_addresses = ["10.0.11.10"]
        }
        translated_packet {
            source {
                dynamic_ip_and_port {
                    interface_address {
                        interface = "${panos_ethernet_interface.eth1_2.name}"
                    }
                }
            }
            destination {
                static {
                    address = "10.0.12.50"
                    port = 80
                }
            }
        }
    }
}

resource "panos_nat_rule_group" "ssh" {
    rule {
        name = "ssh"
        original_packet {
            source_zones = ["${panos_zone.zone_untrust.name}"]
            destination_zone = "${panos_zone.zone_untrust.name}"
            destination_interface = "${panos_ethernet_interface.eth1_1.name}"
            source_addresses = ["any"]
            service = "service-tcp-22"
            destination_addresses = ["10.0.11.10"]
        }
        translated_packet {
            source {
                dynamic_ip_and_port {
                    interface_address {
                        interface = "${panos_ethernet_interface.eth1_2.name}"
                    }
                }
            }
            destination {
                static {
                    address = "10.0.12.50"
                    port = 22
                }
            }
        }
    }
}


resource "panos_nat_rule_group" "outbound" {
    rule {
        name = "outbound"
        original_packet {
            source_zones = ["${panos_zone.zone_trust.name}"]
            destination_zone = "${panos_zone.zone_untrust.name}"
            destination_interface = "${panos_ethernet_interface.eth1_1.name}"
            source_addresses = ["any"]
            destination_addresses = ["any"]
        }
        translated_packet {
            source {
                dynamic_ip_and_port {
                    interface_address {
                        interface = "${panos_ethernet_interface.eth1_1.name}"
                    }
                }
            }
            destination {
            }
        }
    }
}
resource "panos_virtual_router" "vr1" {
  name       = "default"
  interfaces = ["${panos_ethernet_interface.eth1_1.name}", "${panos_ethernet_interface.eth1_2.name}"]
}

resource "panos_static_route_ipv4" "default" {
    name = "localnet"
    virtual_router = "${panos_virtual_router.vr1.name}"
    destination = "0.0.0.0/0"
    interface = "${panos_ethernet_interface.eth1_1.name}"
    next_hop = "10.0.11.1"
}

